CMDBase CLI
===========

:warning: This module was merged in [CMDBase](https://gitlab.com/ipamo/cmdbase) Gitlab repository and is now published on PyPI as [cmdbase-utils](https://pypi.org/project/cmdbase-utils/).
